## Rodis


- [index](https://tapazukk.gitlab.io/rodis)
- [product](https://tapazukk.gitlab.io/rodis/product.html)
- [catalog](https://tapazukk.gitlab.io/rodis/catalog.html)
- [collection](https://tapazukk.gitlab.io/rodis/collection.html)
- [about us](https://tapazukk.gitlab.io/rodis/aboutus.html)
- [store](https://tapazukk.gitlab.io/rodis/store.html)
- [customer-review](https://tapazukk.gitlab.io/rodis/customer-review.html)
- [personal-area](https://tapazukk.gitlab.io/rodis/personal-area.html)
- [news page (main)](https://tapazukk.gitlab.io/rodis/news-main.html)
- [news single page](https://tapazukk.gitlab.io/rodis/news-single-page.html)
- [rules](https://tapazukk.gitlab.io/rodis/rules.html)
- [individual](https://tapazukk.gitlab.io/rodis/individual.html)